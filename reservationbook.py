#!/usr/bin/env python

# [START imports]
import cgi
import urllib
import os
import time
import uuid

from google.appengine.api import users
from google.appengine.ext import ndb

from datetime import datetime,date,timedelta
from time import sleep,mktime
from email import utils

from xml.etree.ElementTree import Element, SubElement, Comment
from xml.etree import ElementTree
from xml.dom import minidom

import webapp2
import jinja2
# [END imports]


JINJA_ENVIRONMENT = jinja2.Environment(
	loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
	extensions=['jinja2.ext.autoescape'],
	autoescape=True)


def prettify(elem):
    """Return a pretty-printed XML string for the Element."""
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml()
	
	
class Author(ndb.Model):
	"""Sub model to represent an author"""
	identity = ndb.StringProperty(indexed=False)
	email = ndb.StringProperty(indexed=False)
	
class Resource(ndb.Model):
	"""Sub model to represent a resource"""
	author = ndb.StructuredProperty(Author)
	uuid = ndb.StringProperty(indexed=True)
	resourceName = ndb.StringProperty(indexed=True)
	date = ndb.DateTimeProperty(auto_now_add=False)	
	startTime = ndb.DateTimeProperty(auto_now_add=False)
	endTime = ndb.DateTimeProperty(auto_now_add=False)
	tags = ndb.StringProperty(repeated=True)
	pubDate = ndb.DateTimeProperty(auto_now_add=True)
	numReservations  = ndb.IntegerProperty(indexed=False)
	
class Reservation(ndb.Model):
	"""Main model for representing an individual Reservation"""
	author=ndb.StructuredProperty(Author)
	uuid = ndb.StringProperty(indexed=True)
	resourceUUID = ndb.StringProperty(indexed=True)
	resourceName = ndb.StringProperty(indexed=False)
	startTime = ndb.DateTimeProperty(auto_now_add=False)
	endTime = ndb.DateTimeProperty(auto_now_add=False)	
	duration = ndb.StringProperty(indexed=False)
	pubDate = ndb.DateTimeProperty(auto_now_add=True)
	
	
class HomePage(webapp2.RequestHandler):

	def get(self):
		resource_query=Resource.query().order(-Resource.date)
		resources=resource_query.fetch()
		
		reservation_query=Reservation.query()
		reservations = reservation_query.fetch()
		
		menu_name = 'your_reservations'
		
		setDeleteID = ''
		getDeleteID = self.request.get('deleteReservation')

		if getDeleteID != '':
			setDeleteID = getDeleteID

			reservation_query_delete = Reservation.query(Reservation.uuid == setDeleteID)
			reservation_key_delete = reservation_query_delete.fetch()
			
			resource_query = Resource.query(Resource.uuid == reservation_key_delete[0].resourceUUID)
			resource = resource_query.fetch()
			resource = resource[0]	
			resource.numReservations  = resource.numReservations  - 1
			resource.put()

			reservation_key_delete[0].key.delete()

			sleep(3)
			query_params = {'menu_name': menu_name}
			self.redirect('/?'+urllib.urlencode(query_params))
		
		get_menu_name = self.request.get('menu_name')
		
		setIsOwnerResource = 'false'
		getIsOwnerResource = self.request.get('isOwnerResource') 
		if getIsOwnerResource != '' :
			setIsOwnerResource = getIsOwnerResource

		setOwner = ''		
		getOwner = self.request.get('owner')
		if getOwner != '':
			setIsOwnerResource = 'true'
			setOwner = getOwner
		
		if get_menu_name != '':
			menu_name = get_menu_name
			
		user = users.get_current_user()

		if user:
			if user:
				url = users.create_logout_url(self.request.uri)
				url_linktext = 'LOGOUT'
				url_createResource = 'ADD RESOURCE'

			template_values = {
				'user': user,
				'url':url,
				'resources':resources,
				'reservations':reservations,
				'url_linktext': url_linktext,
				'url_createResource': url_createResource,
				'menu_name': urllib.quote_plus(menu_name),
				'owner': setOwner,
				'isOwnerResource': setIsOwnerResource,
			}			
			
			template = JINJA_ENVIRONMENT.get_template('index.html')
			self.response.write(template.render(template_values))

		else:
			self.redirect(users.create_login_url(self.request.uri))		


class AddResource(webapp2.RequestHandler):

	def post(self):
		resource_uuid = self.request.get('resource_uuid')
		resource_name = self.request.get('resource_name')

		user = users.get_current_user().user_id()
		reservationbook_name = resource_name + user

		currDay = datetime.now().day
		currMonth = datetime.now().month
		currYear = datetime.now().year
		
		getDay = self.request.get('Day')
		getMonth = self.request.get('Month')
		getYear = self.request.get('Year')
		getDate = getMonth + '/' + getDay + '/' + getYear
		
		getStartHours = self.request.get('startHours')
		getStartMins = self.request.get('startMins')
		getStartMeridian = self.request.get('startMeridian')

		getStartTime = getStartHours + ':' + getStartMins + ' ' + getStartMeridian
		getStartDateTime = getDate + ' ' + getStartTime
		startDateTime = datetime.strptime(getStartDateTime, '%m/%d/%Y %I:%M %p')
		
		getEndHours = self.request.get('endHours')
		getEndMins = self.request.get('endMins')
		getEndMeridian = self.request.get('endMeridian')

		getEndTime = getEndHours + ':' + getEndMins + ' ' + getEndMeridian		
		getEndDateTime = getDate + ' ' + getEndTime
		endDateTime = datetime.strptime(getEndDateTime, '%m/%d/%Y %I:%M %p')
		
		getTags = self.request.get('resource_Tag')
		getTagsList = getTags.split(';')		
		
		if resource_uuid != "":
			resource_query = Resource.query(Resource.uuid == resource_uuid)
			resource_key_temp = resource_query.fetch()
			resource_key_temp[0].resourceName = resource_name
			resource_key_temp[0].startTime = startDateTime
			resource_key_temp[0].endTime = endDateTime
			resource_key_temp[0].tags = getTagsList
			resource_key_temp[0].put()
		
		else:
			addResource = Resource()
			addResource.resourceName = resource_name
			addResource.startTime = startDateTime
			addResource.endTime = endDateTime
			addResource.tags = getTagsList

			if users.get_current_user():
				addResource.author = Author(
									identity=users.get_current_user().user_id(),
									email=users.get_current_user().email())
		
			addResource.uuid = str(uuid.uuid4())
			addResource.numReservations  = 0
			addResource.put()
		
		menu_name = 'all_resources'		
		sleep(3)

		query_params={'menu_name': menu_name}
		self.redirect('/?'+urllib.urlencode(query_params))


class ResourcePage(webapp2.RequestHandler):

	def get(self):
		menu_name = 'show_reservations'
		get_menu_name = self.request.get('menu_name')
		flag = self.request.get('flag')
		
		if get_menu_name != '':
			menu_name = get_menu_name

		resource_uuid = self.request.get('resource_uuid')		
		resource_query = Resource.query(Resource.uuid == resource_uuid)
		resource = resource_query.fetch()
		resource_date = datetime.strftime(resource[0].startTime,'%m/%d/%Y')

		reservation_query = Reservation.query(Reservation.resourceUUID == resource_uuid)
		reservations = reservation_query.fetch()
		
		user = users.get_current_user()

		currDateTime = datetime.now()
		
		if user:
			if user:
				url=users.create_logout_url(self.request.uri)
				url_linktext='LOGOUT'

			template_values={
				'user': user,
				'resource':resource[0],	
				'url':url,
				'url_linktext': url_linktext,
				'menu_name': menu_name,
				'resource_date': resource_date,
				'reservations': reservations,
				'flag': flag,
				'currDateTime': 'currDateTime',
			}

			template = JINJA_ENVIRONMENT.get_template('resource.html')
			self.response.write(template.render(template_values))

		else:
			self.redirect(users.create_login_url(self.request.uri))


class AddReservation(webapp2.RequestHandler):

	def post(self):		
		menu_name = 'your_reservations'
		
		resource_name = self.request.get('resource_name')
		resource_uuid = self.request.get('resource_uuid')
		resource_startDate = self.request.get('resource_startDate')
		
		user = users.get_current_user()
		
		resource_query = Resource.query(Resource.uuid == resource_uuid)
		resource = resource_query.fetch()		
		resource = resource[0]
		resource.date = datetime.now()	
		resource.put()

		sleep(3)
		
		reservation_query = Reservation.query(Reservation.resourceUUID == resource_uuid)
		reservations = reservation_query.fetch()
		
		getStartHours = self.request.get('startHours')
		getStartMins = self.request.get('startMins')
		getStartMeridian = self.request.get('startMeridian')

		getStartTime = getStartHours + ':' + getStartMins + ' ' + getStartMeridian
		getStartDateTime = resource_startDate + ' ' + getStartTime

		startDateTime = datetime.strptime(getStartDateTime, '%m/%d/%Y %I:%M %p')

		getEndHours = self.request.get('endHours')
		getEndMins = self.request.get('endMins')
		getEndTime = getEndHours + ':' + getEndMins	

		endTime = datetime.strptime(getEndTime, '%H:%M')

		delta = timedelta(hours=endTime.hour,minutes=endTime.minute)		
		endDateTime = startDateTime + delta

		flag = 'true'
		currDateTime = datetime.now()
		
		if resource.startTime <= startDateTime:
			if resource.endTime >= endDateTime:
				for reservation in reservations:
					if reservation.startTime > startDateTime and reservation.startTime >= endDateTime:
						flag = 'true'
					else:
						if startDateTime >= reservation.endTime and endDateTime > reservation.endTime:
							flag = 'true'
						else:
							flag = 'false'
			else :
				flag = 'false'
		else :
			flag = 'false'
		
			
		if flag == 'true':
			resource.numReservations  = resource.numReservations  + 1
			resource.put()

			addReservation = Reservation()
			addReservation.resourceName = resource_name
			addReservation.resourceUUID = resource_uuid
			addReservation.startTime = startDateTime
			addReservation.endTime = endDateTime		
			addReservation.duration = getEndTime
			
			if users.get_current_user():
				addReservation.author = Author(
							identity=users.get_current_user().user_id(),
							email=users.get_current_user().email())
			
			addReservation.uuid = str(uuid.uuid4())			
			addReservation.put()

			sleep(3)

			query_params={'menu_name':menu_name}
			self.redirect('/?'+urllib.urlencode(query_params))

		else :
			menu_name = 'add_reservation'

			query_params={'menu_name':menu_name,'flag':flag,'resource_uuid':resource_uuid}
			self.redirect('/resource?'+urllib.urlencode(query_params))


class TagPage(webapp2.RequestHandler):

	def get(self):
		get_Tag = self.request.get('get_Tag')

		resource_query = Resource.query(Resource.tags == get_Tag)
		resources = resource_query.fetch()
			
		user = users.get_current_user()
		
		if user:
			url = users.create_logout_url(self.request.uri)
			url_linktext = 'LOGOUT'
			
			template_values = {
				'resources':resources,
				'user':user,
				'url':url,
				'url_linktext':url_linktext,
			}

			template = JINJA_ENVIRONMENT.get_template('tag.html')
			self.response.write(template.render(template_values))

		else:
			self.redirect(users.create_login_url(self.request.uri))


class RssPage(webapp2.RequestHandler):

	def get(self):
		resource_uuid = self.request.get('get_ResourceID')
		
		reservation_query = Reservation.query(Reservation.resourceUUID == resource_uuid)
		reservations = reservation_query.fetch()

		user = users.get_current_user()

		resource_query = Resource.query(Resource.uuid == resource_uuid)
		resource = resource_query.fetch()

		weburl = self.request.url
		urlstringsplit = weburl.split("/")
		urlstringsplit.remove(urlstringsplit[len(urlstringsplit)-1])

		strjoin = "/".join(urlstringsplit)		
		strjoin = strjoin + "/resource?resource_uuid=" + resource_uuid		
		
		top = Element('rss')
		top.set('version', '2.0')

		channel = SubElement(top, 'channel')

		title = SubElement(channel, 'title')
		title.text = resource[0].resourceName

		description = SubElement(channel, 'description')
		description.text = "This resource is owned by " + resource[0].author.email + " from " + str(resource[0].startTime) + " to " + str(resource[0].endTime)

		link = SubElement(channel, 'link')
		link.text = strjoin

		lastBuildDate = SubElement(channel, 'lastBuildDate')		
		resourcepubdate  = resource[0].pubDate.timetuple()
		pubtimestamp = time.mktime(resourcepubdate)		

		if resource[0].date is None:
			lastBuildDate.text = utils.formatdate(pubtimestamp)

		else:
			resourcelastdate = resource[0].date.timetuple()
			lasttimestamp = time.mktime(resourcelastdate)
			lastBuildDate.text = utils.formatdate(lasttimestamp)
		
		pubDate = SubElement(channel, 'pubDate')
		pubDate.text = utils.formatdate(pubtimestamp)

		count = 1
		for reservation in reservations:
			reservationpubdate  = reservation.pubDate.timetuple()
			reservepubtimestamp = time.mktime(reservationpubdate)

			item = SubElement(channel, 'item')

			title = SubElement(item, 'title')
			title.text = "Reservation" + str(count)

			description = SubElement(item, 'description')
			description.text = "This reservation is done by " + reservation.author.email + " with start time " + str(reservation.startTime) + " for duration " + str(reservation.duration)
			
			link = SubElement(item, 'link')
			link.text = strjoin

			guid = SubElement(item, 'guid')
			guid.set('isPermaLink','false')
			guid.text = reservation.uuid

			pubDate = SubElement(item, 'pubDate')
			pubDate.text = utils.formatdate(reservepubtimestamp)
			count = count + 1
		
		xmlFile = prettify(top)
		
		if user:
			url=users.create_logout_url(self.request.uri)
			url_linktext='LOGOUT'
			
			template_values = {
				'reservations':reservations,
				'user':user,
				'xmlFile':xmlFile,
			}

			template = JINJA_ENVIRONMENT.get_template('rss.html')
			self.response.write(template.render(template_values))

		else:
			self.redirect(users.create_login_url(self.request.uri))


class SearchPage(webapp2.RequestHandler):

	def get(self):
			resource_name = self.request.get('searchResource')
			resource_query = Resource.query(Resource.resourceName == resource_name)
			resources = resource_query.fetch()

			user = users.get_current_user()

			if user:
				url=users.create_logout_url(self.request.uri)
				url_linktext='LOGOUT'
			
				template_values={
						'user':user,
						'resources':resources,
						'url':url,
						'url_linktext':url_linktext
				}

				template = JINJA_ENVIRONMENT.get_template('search.html')
				self.response.write(template.render(template_values))

			else:
				self.redirect(users.create_login_url(self.request.uri))
	
		
app = webapp2.WSGIApplication([
	('/', HomePage),
	('/addResource', AddResource),
	('/resource', ResourcePage),
	('/addReservation', AddReservation),
	('/tag', TagPage),
	('/rss', RssPage),
	('/search', SearchPage),

],debug=True)