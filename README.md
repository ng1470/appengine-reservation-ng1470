OST Final Project
by ng1470@nyu.edu

The goal of the final project is to demonstrate your knowledge of Open Source tools. We will be writing a full featured web-based application, particularly one that implements something akin to Open Table, where instead of just having restaurants, users can add any "resource" that is reservable (e.g. classroom, library book, car share, video game, etc).

Some base requirements for this project:
It must be written in Python and deployed with either App Engine or Heroku.
It must be developed using GIT, with regular code commits.
It must store data in Google App Engine's data store (or MySQL for Heroku).
It must render HTML with templates, preferably Jinja2 or Django.
